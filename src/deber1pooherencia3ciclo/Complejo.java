/*
 * (Números complejos) Cree una clase llamada Complejo para realizar operaciones aritméticas 
con números complejos. Estos números tienen la forma 
    parte Real + parte lmaginaria * i 
en donde i es sqrt(-1)
Escriba un programa para probar su clase. Use variables de punto flotante para representar 
los datos private de la clase. Proporcione un constructor que permita que un objeto de esta clase 
se inicialice al declararse. Proporcione un constructor sin argumentos con valores predeterminados, 
en caso de que no se proporcionen inicializadores. Ofrezca métodos public que realicen las siguientes 
operadones: 
a) Sumar dos números Complejo: las partes reales se suman entre sí y las partes imaginarias también. 
b) Restar dos números Complejo: la parte real del operando derecho se resta de la parte real 
del operando izquierdo, y la parte imaginaria del operando derecho se resta de la parte imaginaria del
operando izquierdo. 
c) Imprimir números Complejo en la forma (parte Real, partelmaginariá). 
 */
package deber1pooherencia3ciclo;
public class Complejo {
    private float real;
    private float imaginario;
    public Complejo(){
    }
    public Complejo(float real, float imaginario) {
        this.real = real;
        this.imaginario = imaginario;
    }
    public Complejo Sumar(Complejo C1, Complejo C2){
        Complejo R = new Complejo();
            R.real=C1.real+C2.real;
            R.imaginario=C1.imaginario+C2.imaginario;
        return R;
    }  
    public Complejo Sumar(Complejo c){
        Complejo s= new Complejo();
        s.real=this.real+c.real;
        s.imaginario=this.imaginario+c.imaginario;
        return s;
    }
    public Complejo Restar(Complejo C1, Complejo C2){
        Complejo R = new Complejo();
        R.real = C1.real - C2.real;
        R.imaginario = C1.imaginario - C2.imaginario;
        return R;
    }
    public void Mostrar(){
        System.out.println("("+real+" + "+imaginario+"i"+")");
    }
}
class PruebaComplejo{
    public static void main(String[] args) {
        Complejo C1 = new Complejo(5,7);
        Complejo C2 = new Complejo(8,5);
        Complejo R = new Complejo();
        R = C1.Sumar(C2);
        System.out.println("El resultado de la suma es: ");
        R.Mostrar();
        R = C1.Restar(C1, C2);
        System.out.println("El resultado de la resta es: ");
        R.Mostrar();
    }
}