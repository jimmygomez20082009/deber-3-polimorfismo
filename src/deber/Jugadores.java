package deber3poliformismo;
import java.util.ArrayList;
import java.util.Scanner;
public abstract class Jugadores{
    int valoracion ;
    public abstract int valoracion();
}
class Equipo extends Jugadores{
    public int goles;
    public int valoracionGoles;
    public Equipo(int goles){
        this.goles=goles;
    }
    @Override
    public  int valoracion(){
        valoracionGoles = goles*30;
        return valoracionGoles;
    }          
}
class Porteros extends Equipo{
    int atajadas;
    public Porteros(int atajadas, int goles) {
        super(goles);
        this.atajadas = atajadas;
    }
    @Override
    public int valoracion(){
        valoracion = super.valoracion()+atajadas*5;
        return valoracion;
    } 
    @Override
    public String toString() {
        return "la valoracion del arquero es: "+ valoracion();
    }  
}
class Atacantes extends Equipo{
     int pasesExitosos;
     int balonesRecuperados;
    public Atacantes(int pasesExitosos, int balonesRecuperados, int goles) {
        super(goles);
        this.pasesExitosos = pasesExitosos;
        this.balonesRecuperados = balonesRecuperados;
    }
     @Override
    public int valoracion(){
        valoracion = super.valoracion()+pasesExitosos*2+balonesRecuperados*3;
        return valoracion;
    }   
     @Override
    public String toString(){
        return "la valoracion del Atacante es: "+ valoracion();
    }
}
class Defensores extends Equipo{
    int pasesExitosos;
    int balonesRecuperados;

    public Defensores(int pasesExitosos, int balonesRecuperados, int goles) {
        super(goles);
        this.pasesExitosos = pasesExitosos;
        this.balonesRecuperados = balonesRecuperados;
    }
       public int valoracion(){
        valoracion = super.valoracion()+pasesExitosos+balonesRecuperados*4;
        return valoracion;
    }    
     @Override
    public String toString(){
        return "la valoracion del defensor es: "+ valoracion();
    }
}
class PruebaJugadores{
    public static ArrayList <Jugadores> jugador = new ArrayList <Jugadores> ();
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int opc;
        do {
            System.out.println("QUE DESEA HACER: ");
            System.out.println("1. Agregar jugadores ");
            System.out.println("2. Conocer el resultado del equipo");
            System.out.println("3. Anotar gol");
            System.out.println("4. Anotar un pase ");
            System.out.println("5. Recuperacion de jugador con dorsal especificado");
            System.out.println("6. Salir ");
            System.out.print("> > > > "); opc = teclado.nextInt();
            switch (opc){
                case 1:
                    agregarJugadores();
                case 2:
                    resultadoEquipo();
                    break;
            }
        } while (opc==6);
    }
    public static void agregarJugadores(){
        Scanner teclado = new Scanner(System.in);
        int[] jugadores = new int [11]; 
        int Njugadores, Camiseta;
        System.out.print("Cuanto jugadores desea agregar: "); Njugadores = teclado.nextInt();
        for (int i = 0; i < Njugadores; i++) {
            System.out.println("Ingrese Numero camiseta del jugador: "); Camiseta = teclado.nextInt();
            for (int j = 0; j < jugadores.length; j++) {
                if (Camiseta==(jugadores[i])) {
                    System.out.println("Ya registrado, Otro numero de camiseta");
                }
            }
            jugadores [i] = Camiseta;
        }
    }
    public static void resultadoEquipo(){
        Scanner teclado = new Scanner(System.in);
        int atajadas, goles, pasesExitosos, balonesRecuperados;
        System.out.print("Nuemro de tajadas del portero: "); atajadas = teclado.nextInt();
        System.out.print("Numero de goles del portero: "); goles = teclado.nextInt();
        Jugadores Porteros = new Porteros (atajadas, goles);  
        System.out.print("Numero de Pases Exitosos del Atacante: "); pasesExitosos = teclado.nextInt();
        System.out.print("Numero de Balones Recuperados del Atacante: "); balonesRecuperados= teclado.nextInt();
        System.out.print("Numero de goles del Atacante: "); goles = teclado.nextInt();
        Jugadores Atacantes = new Atacantes (pasesExitosos, balonesRecuperados, goles);
        System.out.print("Numero de Pases Exitosos del Atacante: "); pasesExitosos = teclado.nextInt();
        System.out.print("Numero de Balones Recuperados del Atacante: "); balonesRecuperados= teclado.nextInt();
        System.out.print("Numero de goles del Atacante: "); goles = teclado.nextInt();
        Jugadores Defensores = new Defensores (pasesExitosos, balonesRecuperados, goles);
        jugador.add(Porteros);
        jugador.add(Atacantes);
        jugador.add(Defensores);
            for (Jugadores jug : jugador) {
            System.out.println(jug);
            }
        }
    public static void anotarGol(){
        Scanner teclado = new Scanner(System.in);
        int goles;        
    }
}
